#include <atomic>

template <typename T>
class tagged_ptr {
private:
    std::atomic<__int128> markedptr;
    std::atomic<T*> ptr;
    std::atomic<bool> mark;
    __int128 mark_ptr(T *ptr, bool mark);
    T* getaddr(__int128 tagged);
    bool getmark(__int128 tagged);
public:
    TaggedPtr(): ptr(NULL), mark(0), markedptr(0) {}

    bool compareAndSet(T* expected_ptr, T* new_ref, bool expected_mark, bool new_mark);
    bool attemptMark(T expected, bool new_mark);
    T get();
}

template <typename T>
__int128 tagged_ptr<T>::mark_ptr(T *ptr, bool mark) {
    return (((__int128)mark << 64) | (__int128)ptr);
}

template <typename T>
void* tagged_ptr<T>::getaddr(__int128 tagged) {
    return (void *)(tagged << 64 >> 64);
}

template <typename T>
bool tagged_ptr<T>::getmark(__int128 tagged) {
    return (bool)(tagged >> 64);
}

template <typename T>
bool tagged_ptr<T>::compareAndSet(T* expected_ptr, T* new_ptr, bool expected_mark, bool new_mark) {
    __int128 expected_tp, new_tp;
    expected_tp = mark_ptr(expected_ptr, expected_mark);
    new_tp = mark_ptr(new_ptr, new_mark);
    return markedptr.compare_exchange_strong(expected_tp, new_tp);
}

template <typename T>
bool tagged_ptr<T>::attemptMark(T* expected_ptr, bool new_mark) {
    __int128 expected_tp, new_tp;
    // Note: What should be the behavior of this method? For now I'll only mark unmarked nodes.
    //       Need to write inline assembly to only use pointer address for comparison.
    expected_tp = mark_ptr(expected_ptr, false);
    new_tp = mark_ptr(expected_ptr, new_mark);
    return markedptr.compare_exchange_strong(expected_tp, new_tp);
}
