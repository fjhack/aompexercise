#include <atomic>
#include <cstdio>

int main(int argc, char** argv)
{
    std::atomic<__int128> aa, ab;
    __int128 av = 0;
    aa = 0;
    ab = 1;
    printf("%llx\n", aa.load());
    printf("Is lock free: %d\n", aa.is_lock_free());
    aa.compare_exchange_strong(av, ab);
    
    printf("%llx\n", aa.load());
    return 0;
}
