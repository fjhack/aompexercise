	.file	"cx16test.cpp"
# GNU C++ (Ubuntu 4.8.4-2ubuntu1~14.04.1) version 4.8.4 (x86_64-linux-gnu)
#	compiled by GNU C version 4.8.4, GMP version 5.1.3, MPFR version 3.1.2-p3, MPC version 1.0.1
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -imultiarch x86_64-linux-gnu -D_GNU_SOURCE cx16test.cpp
# -mcx16 -mtune=generic -march=x86-64 -g -Wformat=0 -std=c++11
# -fverbose-asm -fstack-protector
# options enabled:  -faggressive-loop-optimizations
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg -fcommon
# -fdelete-null-pointer-checks -fdwarf2-cfi-asm -fearly-inlining
# -feliminate-unused-debug-types -fexceptions -ffunction-cse -fgcse-lm
# -fgnu-runtime -fgnu-unique -fident -finline-atomics -fira-hoist-pressure
# -fira-share-save-slots -fira-share-spill-slots -fivopts
# -fkeep-static-consts -fleading-underscore -fmath-errno
# -fmerge-debug-strings -fmove-loop-invariants -fpeephole
# -fprefetch-loop-arrays -freg-struct-return
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fshow-column -fsigned-zeros
# -fsplit-ivs-in-unroller -fstack-protector -fstrict-volatile-bitfields
# -fsync-libcalls -ftrapping-math -ftree-coalesce-vars -ftree-cselim
# -ftree-forwprop -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pta
# -ftree-reassoc -ftree-scev-cprop -ftree-slp-vectorize
# -ftree-vect-loop-version -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -maccumulate-outgoing-args -malign-stringops -mcx16 -mfancy-math-387
# -mfp-ret-in-387 -mfxsr -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4
# -mpush-args -mred-zone -msse -msse2 -mtls-direct-seg-refs

	.text
.Ltext0:
	.section	.text._ZNSt6atomicInEC2Ev,"axG",@progbits,_ZNSt6atomicInEC5Ev,comdat
	.align 2
	.weak	_ZNSt6atomicInEC2Ev
	.type	_ZNSt6atomicInEC2Ev, @function
_ZNSt6atomicInEC2Ev:
.LFB328:
	.file 1 "/usr/include/c++/4.8/atomic"
	.loc 1 167 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# this, this
	.loc 1 167 0
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE328:
	.size	_ZNSt6atomicInEC2Ev, .-_ZNSt6atomicInEC2Ev
	.weak	_ZNSt6atomicInEC1Ev
	.set	_ZNSt6atomicInEC1Ev,_ZNSt6atomicInEC2Ev
	.section	.rodata
.LC0:
	.string	"%llx\n"
.LC1:
	.string	"Is lock free: %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB326:
	.file 2 "cx16test.cpp"
	.loc 2 5 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%rbx	#
	subq	$72, %rsp	#,
	.cfi_offset 3, -24
	movl	%edi, -68(%rbp)	# argc, argc
	movq	%rsi, -80(%rbp)	# argv, argv
.LBB2:
	.loc 2 6 0
	leaq	-64(%rbp), %rax	#, tmp66
	movq	%rax, %rdi	# tmp66,
	call	_ZNSt6atomicInEC1Ev	#
	leaq	-48(%rbp), %rax	#, tmp67
	movq	%rax, %rdi	# tmp67,
	call	_ZNSt6atomicInEC1Ev	#
	.loc 2 7 0
	movq	$0, -32(%rbp)	#, av
	movq	$0, -24(%rbp)	#, av
	.loc 2 8 0
	movl	$0, %ecx	#, tmp68
	movl	$0, %edx	#, tmp69
	leaq	-64(%rbp), %rax	#, tmp70
	movq	%rcx, %rsi	# tmp68,
	movq	%rax, %rdi	# tmp70,
	call	_ZNSt6atomicInEaSEn	#
	.loc 2 9 0
	movl	$1, %ecx	#, tmp71
	movl	$0, %edx	#, tmp72
	leaq	-48(%rbp), %rax	#, tmp73
	movq	%rcx, %rsi	# tmp71,
	movq	%rax, %rdi	# tmp73,
	call	_ZNSt6atomicInEaSEn	#
	.loc 2 10 0
	leaq	-64(%rbp), %rax	#, tmp74
	movl	$5, %esi	#,
	movq	%rax, %rdi	# tmp74,
	call	_ZNKSt6atomicInE4loadESt12memory_order	#
	movq	%rax, %rcx	# D.8544, tmp75
	movq	%rdx, %rbx	# D.8544,
	movq	%rdx, %rax	#, tmp78
	movq	%rcx, %rsi	# tmp77,
	movq	%rax, %rdx	# tmp78,
	movl	$.LC0, %edi	#,
	movl	$0, %eax	#,
	call	printf	#
	.loc 2 11 0
	leaq	-64(%rbp), %rax	#, tmp79
	movq	%rax, %rdi	# tmp79,
	call	_ZNKSt6atomicInE12is_lock_freeEv	#
	movzbl	%al, %eax	# D.8545, D.8546
	movl	%eax, %esi	# D.8546,
	movl	$.LC1, %edi	#,
	movl	$0, %eax	#,
	call	printf	#
	.loc 2 12 0
	leaq	-48(%rbp), %rax	#, tmp80
	movq	%rax, %rdi	# tmp80,
	call	_ZNKSt6atomicInEcvnEv	#
	leaq	-32(%rbp), %rsi	#, tmp81
	leaq	-64(%rbp), %rdi	#, tmp82
	movl	$5, %r8d	#,
	movq	%rdx, %rcx	# D.8544,
	movq	%rax, %rdx	# D.8544,
	call	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order	#
	.loc 2 14 0
	leaq	-64(%rbp), %rax	#, tmp83
	movl	$5, %esi	#,
	movq	%rax, %rdi	# tmp83,
	call	_ZNKSt6atomicInE4loadESt12memory_order	#
	movq	%rax, %rcx	# D.8544, tmp84
	movq	%rdx, %rbx	# D.8544,
	movq	%rdx, %rax	#, tmp87
	movq	%rcx, %rsi	# tmp86,
	movq	%rax, %rdx	# tmp87,
	movl	$.LC0, %edi	#,
	movl	$0, %eax	#,
	call	printf	#
	.loc 2 15 0
	movl	$0, %eax	#, D.8546
.LBE2:
	.loc 2 16 0
	addq	$72, %rsp	#,
	popq	%rbx	#
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE326:
	.size	main, .-main
	.section	.text._ZNSt6atomicInEaSEn,"axG",@progbits,_ZNSt6atomicInEaSEn,comdat
	.align 2
	.weak	_ZNSt6atomicInEaSEn
	.type	_ZNSt6atomicInEaSEn, @function
_ZNSt6atomicInEaSEn:
.LFB348:
	.loc 1 182 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$32, %rsp	#,
	movq	%rdi, -8(%rbp)	# this, this
	movq	%rsi, %rax	# __i, tmp62
	movq	%rdx, %rcx	# __i, tmp63
	movq	%rcx, %rdx	# tmp63,
	movq	%rax, -32(%rbp)	# tmp61, __i
	movq	%rdx, -24(%rbp)	#, __i
	.loc 1 183 0
	movq	-32(%rbp), %rsi	# __i, tmp64
	movq	-24(%rbp), %rdx	# __i, tmp65
	movq	-8(%rbp), %rax	# this, tmp66
	movl	$5, %ecx	#,
	movq	%rax, %rdi	# tmp66,
	call	_ZNSt6atomicInE5storeEnSt12memory_order	#
	movq	-32(%rbp), %rax	# __i, D.8549
	movq	-24(%rbp), %rdx	# __i, D.8549
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE348:
	.size	_ZNSt6atomicInEaSEn, .-_ZNSt6atomicInEaSEn
	.section	.text._ZNKSt6atomicInE4loadESt12memory_order,"axG",@progbits,_ZNKSt6atomicInE4loadESt12memory_order,comdat
	.align 2
	.weak	_ZNKSt6atomicInE4loadESt12memory_order
	.type	_ZNKSt6atomicInE4loadESt12memory_order, @function
_ZNKSt6atomicInE4loadESt12memory_order:
.LFB349:
	.loc 1 206 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%rbx	#
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)	# this, this
	movl	%esi, -44(%rbp)	# _m, _m
.LBB3:
	.loc 1 209 0
	movq	-40(%rbp), %rsi	# this, D.8550
	movl	$0, %edx	#, tmp66
	movl	$0, %ecx	#,
	movl	$0, %r10d	#, tmp67
	movl	$0, %r11d	#,
	movq	%rdx, %rax	# tmp66, tmp70
	movq	%rcx, %rdx	#, tmp71
	movq	%r10, %rbx	# tmp67, tmp72
	movq	%r11, %rcx	#, tmp73
	lock cmpxchg16b	(%rsi)	#,* D.8550
	movq	%rdx, %r9	# tmp71, D.8551
	movq	%rax, %r8	# tmp70, D.8551
	movq	%r8, %rax	# D.8551, tmp.4
	movq	%r9, %rdx	# D.8551, tmp.4
	movq	%rax, -32(%rbp)	# tmp.4, tmp
	movq	%rdx, -24(%rbp)	# tmp.4, tmp
	.loc 1 210 0
	movq	-32(%rbp), %rax	# tmp, D.8552
	movq	-24(%rbp), %rdx	# tmp, D.8552
.LBE3:
	.loc 1 211 0
	popq	%rbx	#
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE349:
	.size	_ZNKSt6atomicInE4loadESt12memory_order, .-_ZNKSt6atomicInE4loadESt12memory_order
	.section	.text._ZNKSt6atomicInE12is_lock_freeEv,"axG",@progbits,_ZNKSt6atomicInE12is_lock_freeEv,comdat
	.align 2
	.weak	_ZNKSt6atomicInE12is_lock_freeEv
	.type	_ZNKSt6atomicInE12is_lock_freeEv, @function
_ZNKSt6atomicInE12is_lock_freeEv:
.LFB350:
	.loc 1 190 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# this, this
	.loc 1 191 0
	movl	$1, %eax	#, D.8553
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE350:
	.size	_ZNKSt6atomicInE12is_lock_freeEv, .-_ZNKSt6atomicInE12is_lock_freeEv
	.section	.text._ZNKSt6atomicInEcvnEv,"axG",@progbits,_ZNKSt6atomicInEcvnEv,comdat
	.align 2
	.weak	_ZNKSt6atomicInEcvnEv
	.type	_ZNKSt6atomicInEcvnEv, @function
_ZNKSt6atomicInEcvnEv:
.LFB351:
	.loc 1 175 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$16, %rsp	#,
	movq	%rdi, -8(%rbp)	# this, this
	.loc 1 176 0
	movq	-8(%rbp), %rax	# this, tmp61
	movl	$5, %esi	#,
	movq	%rax, %rdi	# tmp61,
	call	_ZNKSt6atomicInE4loadESt12memory_order	#
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE351:
	.size	_ZNKSt6atomicInEcvnEv, .-_ZNKSt6atomicInEcvnEv
	.section	.text._ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order,"axG",@progbits,_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order,comdat
	.align 2
	.weak	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order
	.type	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order, @function
_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order:
.LFB352:
	.loc 1 277 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$48, %rsp	#,
	movq	%rdi, -8(%rbp)	# this, this
	movq	%rsi, -16(%rbp)	# __e, __e
	movq	%rdx, -32(%rbp)	# __i, __i
	movq	%rcx, -24(%rbp)	# __i, __i
	movl	%r8d, -36(%rbp)	# __m, __m
	.loc 1 279 0
	movl	-36(%rbp), %r8d	# __m, tmp61
	movl	-36(%rbp), %ecx	# __m, tmp62
	movq	-32(%rbp), %rax	# __i, tmp63
	movq	-24(%rbp), %rdx	# __i,
	movq	-16(%rbp), %rsi	# __e, tmp64
	movq	-8(%rbp), %rdi	# this, tmp65
	movl	%r8d, %r9d	# tmp61,
	movl	%ecx, %r8d	# tmp62,
	movq	%rdx, %rcx	#,
	movq	%rax, %rdx	# tmp63,
	call	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_	#
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE352:
	.size	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order, .-_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order
	.section	.text._ZNSt6atomicInE5storeEnSt12memory_order,"axG",@progbits,_ZNSt6atomicInE5storeEnSt12memory_order,comdat
	.align 2
	.weak	_ZNSt6atomicInE5storeEnSt12memory_order
	.type	_ZNSt6atomicInE5storeEnSt12memory_order, @function
_ZNSt6atomicInE5storeEnSt12memory_order:
.LFB353:
	.loc 1 198 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%rbx	#
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)	# this, this
	movq	%rsi, %rax	# __i, tmp63
	movq	%rdx, %rsi	# __i, tmp64
	movq	%rsi, %rdx	# tmp64,
	movq	%rax, -48(%rbp)	# tmp62, __i
	movq	%rdx, -40(%rbp)	#, __i
	movl	%ecx, -28(%rbp)	# _m, _m
	.loc 1 199 0
	movq	-48(%rbp), %r9	# __i, D.8556
	movq	-40(%rbp), %r10	# __i, D.8556
	movq	-24(%rbp), %r8	# this, D.8557
	movq	(%r8), %rsi	#* D.8557, tmp66
	movq	8(%r8), %rdi	#,
.L15:
	movq	%rsi, %rdx	# tmp66, tmp65
	movq	%rdi, %rcx	#,
	movq	%rdx, %rax	# tmp65, tmp70
	movq	%rcx, %rdx	#, tmp71
	movq	%r9, %rbx	# D.8556, tmp72
	movq	%r10, %rcx	# D.8556, tmp73
	lock cmpxchg16b	(%r8)	#,* D.8557
	movq	%rdx, %rdi	# tmp71,
	movq	%rax, %rsi	# tmp70, tmp66
	sete	%al	#, tmp67
	testb	%al, %al	# tmp67
	je	.L15	#,
	popq	%rbx	#
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE353:
	.size	_ZNSt6atomicInE5storeEnSt12memory_order, .-_ZNSt6atomicInE5storeEnSt12memory_order
	.section	.text._ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_,"axG",@progbits,_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_,comdat
	.align 2
	.weak	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_
	.type	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_, @function
_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_:
.LFB354:
	.loc 1 263 0
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%r15	#
	pushq	%r14	#
	pushq	%rbx	#
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 3, -40
	movq	%rdi, -40(%rbp)	# this, this
	movq	%rsi, -48(%rbp)	# __e, __e
	movq	%rdx, -64(%rbp)	# __i, __i
	movq	%rcx, -56(%rbp)	# __i, __i
	movl	%r8d, -68(%rbp)	# __s, __s
	movl	%r9d, -72(%rbp)	# __f, __f
	.loc 1 266 0
	movq	-64(%rbp), %r14	# __i, D.8558
	movq	-56(%rbp), %r15	# __i, D.8558
	movq	-40(%rbp), %rsi	# this, D.8559
	movq	-48(%rbp), %rax	# __e, tmp66
	movq	(%rax), %rcx	#, tmp65
	movq	8(%rax), %rbx	#,
	movq	%rcx, %rax	# tmp65, tmp71
	movq	%rbx, %rdx	#, tmp72
	movq	%r14, %rbx	# D.8558, tmp73
	movq	%r15, %rcx	# D.8558, tmp74
	lock cmpxchg16b	(%rsi)	#,* D.8559
	movq	%rdx, %r11	# tmp72,
	movq	%rax, %r10	# tmp71, tmp67
	sete	%al	#, D.8560
	testb	%al, %al	# D.8560
	jne	.L18	#,
	movq	-48(%rbp), %rdx	# __e, tmp68
	movq	%r10, (%rdx)	# tmp67,
	movq	%r11, 8(%rdx)	#,
.L18:
	.loc 1 267 0 discriminator 1
	popq	%rbx	#
	popq	%r14	#
	popq	%r15	#
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE354:
	.size	_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_, .-_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_
	.text
.Letext0:
	.file 3 "/usr/include/c++/4.8/cstdio"
	.file 4 "/usr/include/c++/4.8/bits/atomic_base.h"
	.file 5 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/libio.h"
	.file 9 "/usr/include/wchar.h"
	.file 10 "/usr/include/_G_config.h"
	.file 11 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xdc9
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF134
	.byte	0x4
	.long	.LASF135
	.long	.LASF136
	.long	.Ldebug_ranges0+0
	.quad	0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF1
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF2
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF4
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF6
	.uleb128 0x4
	.string	"std"
	.byte	0xb
	.byte	0
	.long	0x54f
	.uleb128 0x5
	.long	.LASF52
	.byte	0x4
	.byte	0x4
	.byte	0x34
	.long	0x9d
	.uleb128 0x6
	.long	.LASF7
	.sleb128 0
	.uleb128 0x6
	.long	.LASF8
	.sleb128 1
	.uleb128 0x6
	.long	.LASF9
	.sleb128 2
	.uleb128 0x6
	.long	.LASF10
	.sleb128 3
	.uleb128 0x6
	.long	.LASF11
	.sleb128 4
	.uleb128 0x6
	.long	.LASF12
	.sleb128 5
	.byte	0
	.uleb128 0x7
	.long	.LASF52
	.byte	0x4
	.byte	0x3c
	.long	0x6c
	.uleb128 0x8
	.byte	0x3
	.byte	0x60
	.long	0x5b0
	.uleb128 0x8
	.byte	0x3
	.byte	0x61
	.long	0x83f
	.uleb128 0x8
	.byte	0x3
	.byte	0x63
	.long	0x84a
	.uleb128 0x8
	.byte	0x3
	.byte	0x64
	.long	0x862
	.uleb128 0x8
	.byte	0x3
	.byte	0x65
	.long	0x877
	.uleb128 0x8
	.byte	0x3
	.byte	0x66
	.long	0x88d
	.uleb128 0x8
	.byte	0x3
	.byte	0x67
	.long	0x8a3
	.uleb128 0x8
	.byte	0x3
	.byte	0x68
	.long	0x8b8
	.uleb128 0x8
	.byte	0x3
	.byte	0x69
	.long	0x8ce
	.uleb128 0x8
	.byte	0x3
	.byte	0x6a
	.long	0x8ef
	.uleb128 0x8
	.byte	0x3
	.byte	0x6b
	.long	0x90f
	.uleb128 0x8
	.byte	0x3
	.byte	0x6f
	.long	0x92a
	.uleb128 0x8
	.byte	0x3
	.byte	0x70
	.long	0x94f
	.uleb128 0x8
	.byte	0x3
	.byte	0x72
	.long	0x96f
	.uleb128 0x8
	.byte	0x3
	.byte	0x73
	.long	0x98f
	.uleb128 0x8
	.byte	0x3
	.byte	0x74
	.long	0x9b5
	.uleb128 0x8
	.byte	0x3
	.byte	0x76
	.long	0x9cb
	.uleb128 0x8
	.byte	0x3
	.byte	0x77
	.long	0x9e1
	.uleb128 0x8
	.byte	0x3
	.byte	0x78
	.long	0x9ed
	.uleb128 0x8
	.byte	0x3
	.byte	0x79
	.long	0xa03
	.uleb128 0x8
	.byte	0x3
	.byte	0x7e
	.long	0xa15
	.uleb128 0x8
	.byte	0x3
	.byte	0x7f
	.long	0xa2a
	.uleb128 0x8
	.byte	0x3
	.byte	0x80
	.long	0xa44
	.uleb128 0x8
	.byte	0x3
	.byte	0x82
	.long	0xa56
	.uleb128 0x8
	.byte	0x3
	.byte	0x83
	.long	0xa6d
	.uleb128 0x8
	.byte	0x3
	.byte	0x86
	.long	0xa92
	.uleb128 0x8
	.byte	0x3
	.byte	0x87
	.long	0xa9d
	.uleb128 0x8
	.byte	0x3
	.byte	0x88
	.long	0xab2
	.uleb128 0x9
	.long	.LASF58
	.byte	0x10
	.byte	0x1
	.byte	0xa1
	.long	0x53f
	.uleb128 0xa
	.long	.LASF137
	.byte	0x1
	.byte	0xa4
	.long	0xacd
	.byte	0
	.byte	0x3
	.uleb128 0xb
	.long	.LASF13
	.byte	0x1
	.byte	0xa7
	.long	0x194
	.long	0x19a
	.uleb128 0xc
	.long	0xad4
	.byte	0
	.uleb128 0xb
	.long	.LASF14
	.byte	0x1
	.byte	0xa8
	.long	0x1a9
	.long	0x1b4
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xc
	.long	0x37
	.byte	0
	.uleb128 0xb
	.long	.LASF13
	.byte	0x1
	.byte	0xa9
	.long	0x1c3
	.long	0x1ce
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xada
	.byte	0
	.uleb128 0xe
	.long	.LASF15
	.byte	0x1
	.byte	0xaa
	.long	.LASF16
	.long	0xae0
	.long	0x1e5
	.long	0x1f0
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xada
	.byte	0
	.uleb128 0xe
	.long	.LASF15
	.byte	0x1
	.byte	0xab
	.long	.LASF17
	.long	0xae0
	.long	0x207
	.long	0x212
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xada
	.byte	0
	.uleb128 0xb
	.long	.LASF13
	.byte	0x1
	.byte	0xad
	.long	0x221
	.long	0x22c
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xacd
	.byte	0
	.uleb128 0xe
	.long	.LASF18
	.byte	0x1
	.byte	0xaf
	.long	.LASF19
	.long	0xacd
	.long	0x243
	.long	0x249
	.uleb128 0xc
	.long	0xaec
	.byte	0
	.uleb128 0xe
	.long	.LASF18
	.byte	0x1
	.byte	0xb2
	.long	.LASF20
	.long	0xacd
	.long	0x260
	.long	0x266
	.uleb128 0xc
	.long	0xaf2
	.byte	0
	.uleb128 0xe
	.long	.LASF15
	.byte	0x1
	.byte	0xb6
	.long	.LASF21
	.long	0xacd
	.long	0x27d
	.long	0x288
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xacd
	.byte	0
	.uleb128 0xe
	.long	.LASF15
	.byte	0x1
	.byte	0xba
	.long	.LASF22
	.long	0xacd
	.long	0x29f
	.long	0x2aa
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xacd
	.byte	0
	.uleb128 0xe
	.long	.LASF23
	.byte	0x1
	.byte	0xbe
	.long	.LASF24
	.long	0x54f
	.long	0x2c1
	.long	0x2c7
	.uleb128 0xc
	.long	0xaec
	.byte	0
	.uleb128 0xe
	.long	.LASF23
	.byte	0x1
	.byte	0xc2
	.long	.LASF25
	.long	0x54f
	.long	0x2de
	.long	0x2e4
	.uleb128 0xc
	.long	0xaf2
	.byte	0
	.uleb128 0xf
	.long	.LASF26
	.byte	0x1
	.byte	0xc6
	.long	.LASF27
	.long	0x2f7
	.long	0x307
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xf
	.long	.LASF26
	.byte	0x1
	.byte	0xca
	.long	.LASF28
	.long	0x31a
	.long	0x32a
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xe
	.long	.LASF29
	.byte	0x1
	.byte	0xce
	.long	.LASF30
	.long	0xacd
	.long	0x341
	.long	0x34c
	.uleb128 0xc
	.long	0xaec
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xe
	.long	.LASF29
	.byte	0x1
	.byte	0xd6
	.long	.LASF31
	.long	0xacd
	.long	0x363
	.long	0x36e
	.uleb128 0xc
	.long	0xaf2
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xe
	.long	.LASF32
	.byte	0x1
	.byte	0xde
	.long	.LASF33
	.long	0xacd
	.long	0x385
	.long	0x395
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xe
	.long	.LASF32
	.byte	0x1
	.byte	0xe6
	.long	.LASF34
	.long	0xacd
	.long	0x3ac
	.long	0x3bc
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xe
	.long	.LASF35
	.byte	0x1
	.byte	0xef
	.long	.LASF36
	.long	0x54f
	.long	0x3d3
	.long	0x3ed
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xe
	.long	.LASF35
	.byte	0x1
	.byte	0xf6
	.long	.LASF37
	.long	0x54f
	.long	0x404
	.long	0x41e
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0xe
	.long	.LASF35
	.byte	0x1
	.byte	0xfd
	.long	.LASF38
	.long	0x54f
	.long	0x435
	.long	0x44a
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0x10
	.long	.LASF35
	.byte	0x1
	.value	0x102
	.long	.LASF40
	.long	0x54f
	.long	0x462
	.long	0x477
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0x10
	.long	.LASF39
	.byte	0x1
	.value	0x107
	.long	.LASF41
	.long	0x54f
	.long	0x48f
	.long	0x4a9
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0x10
	.long	.LASF39
	.byte	0x1
	.value	0x10e
	.long	.LASF42
	.long	0x54f
	.long	0x4c1
	.long	0x4db
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0x10
	.long	.LASF39
	.byte	0x1
	.value	0x115
	.long	.LASF43
	.long	0x54f
	.long	0x4f3
	.long	0x508
	.uleb128 0xc
	.long	0xad4
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0x10
	.long	.LASF39
	.byte	0x1
	.value	0x11a
	.long	.LASF44
	.long	0x54f
	.long	0x520
	.long	0x535
	.uleb128 0xc
	.long	0xae6
	.uleb128 0xd
	.long	0xaf8
	.uleb128 0xd
	.long	0xacd
	.uleb128 0xd
	.long	0x9d
	.byte	0
	.uleb128 0x11
	.string	"_Tp"
	.long	0xacd
	.byte	0
	.uleb128 0x12
	.long	0x16c
	.uleb128 0x13
	.long	0x16c
	.uleb128 0x12
	.long	0x544
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x2
	.long	.LASF45
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF46
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF47
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF48
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.long	.LASF49
	.uleb128 0x2
	.byte	0x2
	.byte	0x10
	.long	.LASF50
	.uleb128 0x2
	.byte	0x4
	.byte	0x10
	.long	.LASF51
	.uleb128 0x7
	.long	.LASF53
	.byte	0x5
	.byte	0xd4
	.long	0x5a
	.uleb128 0x7
	.long	.LASF54
	.byte	0x6
	.byte	0x83
	.long	0x3e
	.uleb128 0x7
	.long	.LASF55
	.byte	0x6
	.byte	0x84
	.long	0x3e
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF56
	.uleb128 0x14
	.byte	0x8
	.uleb128 0x15
	.byte	0x8
	.long	0x556
	.uleb128 0x7
	.long	.LASF57
	.byte	0x7
	.byte	0x30
	.long	0x5bb
	.uleb128 0x9
	.long	.LASF59
	.byte	0xd8
	.byte	0x8
	.byte	0xf5
	.long	0x73b
	.uleb128 0x16
	.long	.LASF60
	.byte	0x8
	.byte	0xf6
	.long	0x37
	.byte	0
	.uleb128 0x16
	.long	.LASF61
	.byte	0x8
	.byte	0xfb
	.long	0x5aa
	.byte	0x8
	.uleb128 0x16
	.long	.LASF62
	.byte	0x8
	.byte	0xfc
	.long	0x5aa
	.byte	0x10
	.uleb128 0x16
	.long	.LASF63
	.byte	0x8
	.byte	0xfd
	.long	0x5aa
	.byte	0x18
	.uleb128 0x16
	.long	.LASF64
	.byte	0x8
	.byte	0xfe
	.long	0x5aa
	.byte	0x20
	.uleb128 0x16
	.long	.LASF65
	.byte	0x8
	.byte	0xff
	.long	0x5aa
	.byte	0x28
	.uleb128 0x17
	.long	.LASF66
	.byte	0x8
	.value	0x100
	.long	0x5aa
	.byte	0x30
	.uleb128 0x17
	.long	.LASF67
	.byte	0x8
	.value	0x101
	.long	0x5aa
	.byte	0x38
	.uleb128 0x17
	.long	.LASF68
	.byte	0x8
	.value	0x102
	.long	0x5aa
	.byte	0x40
	.uleb128 0x17
	.long	.LASF69
	.byte	0x8
	.value	0x104
	.long	0x5aa
	.byte	0x48
	.uleb128 0x17
	.long	.LASF70
	.byte	0x8
	.value	0x105
	.long	0x5aa
	.byte	0x50
	.uleb128 0x17
	.long	.LASF71
	.byte	0x8
	.value	0x106
	.long	0x5aa
	.byte	0x58
	.uleb128 0x17
	.long	.LASF72
	.byte	0x8
	.value	0x108
	.long	0x802
	.byte	0x60
	.uleb128 0x17
	.long	.LASF73
	.byte	0x8
	.value	0x10a
	.long	0x808
	.byte	0x68
	.uleb128 0x17
	.long	.LASF74
	.byte	0x8
	.value	0x10c
	.long	0x37
	.byte	0x70
	.uleb128 0x17
	.long	.LASF75
	.byte	0x8
	.value	0x110
	.long	0x37
	.byte	0x74
	.uleb128 0x17
	.long	.LASF76
	.byte	0x8
	.value	0x112
	.long	0x58b
	.byte	0x78
	.uleb128 0x17
	.long	.LASF77
	.byte	0x8
	.value	0x116
	.long	0x4c
	.byte	0x80
	.uleb128 0x17
	.long	.LASF78
	.byte	0x8
	.value	0x117
	.long	0x29
	.byte	0x82
	.uleb128 0x17
	.long	.LASF79
	.byte	0x8
	.value	0x118
	.long	0x80e
	.byte	0x83
	.uleb128 0x17
	.long	.LASF80
	.byte	0x8
	.value	0x11c
	.long	0x81e
	.byte	0x88
	.uleb128 0x17
	.long	.LASF81
	.byte	0x8
	.value	0x125
	.long	0x596
	.byte	0x90
	.uleb128 0x17
	.long	.LASF82
	.byte	0x8
	.value	0x12e
	.long	0x5a8
	.byte	0x98
	.uleb128 0x17
	.long	.LASF83
	.byte	0x8
	.value	0x12f
	.long	0x5a8
	.byte	0xa0
	.uleb128 0x17
	.long	.LASF84
	.byte	0x8
	.value	0x130
	.long	0x5a8
	.byte	0xa8
	.uleb128 0x17
	.long	.LASF85
	.byte	0x8
	.value	0x131
	.long	0x5a8
	.byte	0xb0
	.uleb128 0x17
	.long	.LASF86
	.byte	0x8
	.value	0x132
	.long	0x580
	.byte	0xb8
	.uleb128 0x17
	.long	.LASF87
	.byte	0x8
	.value	0x134
	.long	0x37
	.byte	0xc0
	.uleb128 0x17
	.long	.LASF88
	.byte	0x8
	.value	0x136
	.long	0x824
	.byte	0xc4
	.byte	0
	.uleb128 0x18
	.byte	0x8
	.byte	0x9
	.byte	0x53
	.long	.LASF94
	.long	0x77f
	.uleb128 0x19
	.byte	0x4
	.byte	0x9
	.byte	0x56
	.long	0x766
	.uleb128 0x1a
	.long	.LASF89
	.byte	0x9
	.byte	0x58
	.long	0x53
	.uleb128 0x1a
	.long	.LASF90
	.byte	0x9
	.byte	0x5c
	.long	0x77f
	.byte	0
	.uleb128 0x16
	.long	.LASF91
	.byte	0x9
	.byte	0x54
	.long	0x37
	.byte	0
	.uleb128 0x16
	.long	.LASF92
	.byte	0x9
	.byte	0x5d
	.long	0x747
	.byte	0x4
	.byte	0
	.uleb128 0x1b
	.long	0x556
	.long	0x78f
	.uleb128 0x1c
	.long	0x5a1
	.byte	0x3
	.byte	0
	.uleb128 0x7
	.long	.LASF93
	.byte	0x9
	.byte	0x5e
	.long	0x73b
	.uleb128 0x18
	.byte	0x10
	.byte	0xa
	.byte	0x16
	.long	.LASF95
	.long	0x7bf
	.uleb128 0x16
	.long	.LASF96
	.byte	0xa
	.byte	0x17
	.long	0x58b
	.byte	0
	.uleb128 0x16
	.long	.LASF97
	.byte	0xa
	.byte	0x18
	.long	0x78f
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	.LASF98
	.byte	0xa
	.byte	0x19
	.long	0x79a
	.uleb128 0x1d
	.long	.LASF138
	.byte	0x8
	.byte	0x9a
	.uleb128 0x9
	.long	.LASF99
	.byte	0x18
	.byte	0x8
	.byte	0xa0
	.long	0x802
	.uleb128 0x16
	.long	.LASF100
	.byte	0x8
	.byte	0xa1
	.long	0x802
	.byte	0
	.uleb128 0x16
	.long	.LASF101
	.byte	0x8
	.byte	0xa2
	.long	0x808
	.byte	0x8
	.uleb128 0x16
	.long	.LASF102
	.byte	0x8
	.byte	0xa6
	.long	0x37
	.byte	0x10
	.byte	0
	.uleb128 0x15
	.byte	0x8
	.long	0x7d1
	.uleb128 0x15
	.byte	0x8
	.long	0x5bb
	.uleb128 0x1b
	.long	0x556
	.long	0x81e
	.uleb128 0x1c
	.long	0x5a1
	.byte	0
	.byte	0
	.uleb128 0x15
	.byte	0x8
	.long	0x7ca
	.uleb128 0x1b
	.long	0x556
	.long	0x834
	.uleb128 0x1c
	.long	0x5a1
	.byte	0x13
	.byte	0
	.uleb128 0x15
	.byte	0x8
	.long	0x83a
	.uleb128 0x12
	.long	0x556
	.uleb128 0x7
	.long	.LASF103
	.byte	0x7
	.byte	0x6e
	.long	0x7bf
	.uleb128 0x1e
	.long	.LASF119
	.byte	0x7
	.value	0x33a
	.long	0x85c
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x15
	.byte	0x8
	.long	0x5b0
	.uleb128 0x1f
	.long	.LASF104
	.byte	0x7
	.byte	0xed
	.long	0x37
	.long	0x877
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF105
	.byte	0x7
	.value	0x33c
	.long	0x37
	.long	0x88d
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF106
	.byte	0x7
	.value	0x33e
	.long	0x37
	.long	0x8a3
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x1f
	.long	.LASF107
	.byte	0x7
	.byte	0xf2
	.long	0x37
	.long	0x8b8
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF108
	.byte	0x7
	.value	0x213
	.long	0x37
	.long	0x8ce
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF109
	.byte	0x7
	.value	0x31e
	.long	0x37
	.long	0x8e9
	.uleb128 0xd
	.long	0x85c
	.uleb128 0xd
	.long	0x8e9
	.byte	0
	.uleb128 0x15
	.byte	0x8
	.long	0x83f
	.uleb128 0x20
	.long	.LASF110
	.byte	0x7
	.value	0x26e
	.long	0x5aa
	.long	0x90f
	.uleb128 0xd
	.long	0x5aa
	.uleb128 0xd
	.long	0x37
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF111
	.byte	0x7
	.value	0x110
	.long	0x85c
	.long	0x92a
	.uleb128 0xd
	.long	0x834
	.uleb128 0xd
	.long	0x834
	.byte	0
	.uleb128 0x20
	.long	.LASF112
	.byte	0x7
	.value	0x2c5
	.long	0x580
	.long	0x94f
	.uleb128 0xd
	.long	0x5a8
	.uleb128 0xd
	.long	0x580
	.uleb128 0xd
	.long	0x580
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF113
	.byte	0x7
	.value	0x116
	.long	0x85c
	.long	0x96f
	.uleb128 0xd
	.long	0x834
	.uleb128 0xd
	.long	0x834
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF114
	.byte	0x7
	.value	0x2ed
	.long	0x37
	.long	0x98f
	.uleb128 0xd
	.long	0x85c
	.uleb128 0xd
	.long	0x3e
	.uleb128 0xd
	.long	0x37
	.byte	0
	.uleb128 0x20
	.long	.LASF115
	.byte	0x7
	.value	0x323
	.long	0x37
	.long	0x9aa
	.uleb128 0xd
	.long	0x85c
	.uleb128 0xd
	.long	0x9aa
	.byte	0
	.uleb128 0x15
	.byte	0x8
	.long	0x9b0
	.uleb128 0x12
	.long	0x83f
	.uleb128 0x20
	.long	.LASF116
	.byte	0x7
	.value	0x2f2
	.long	0x3e
	.long	0x9cb
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x20
	.long	.LASF117
	.byte	0x7
	.value	0x214
	.long	0x37
	.long	0x9e1
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x21
	.long	.LASF126
	.byte	0x7
	.value	0x21a
	.long	0x37
	.uleb128 0x20
	.long	.LASF118
	.byte	0x7
	.value	0x27e
	.long	0x5aa
	.long	0xa03
	.uleb128 0xd
	.long	0x5aa
	.byte	0
	.uleb128 0x1e
	.long	.LASF120
	.byte	0x7
	.value	0x34e
	.long	0xa15
	.uleb128 0xd
	.long	0x834
	.byte	0
	.uleb128 0x1f
	.long	.LASF121
	.byte	0x7
	.byte	0xb2
	.long	0x37
	.long	0xa2a
	.uleb128 0xd
	.long	0x834
	.byte	0
	.uleb128 0x1f
	.long	.LASF122
	.byte	0x7
	.byte	0xb4
	.long	0x37
	.long	0xa44
	.uleb128 0xd
	.long	0x834
	.uleb128 0xd
	.long	0x834
	.byte	0
	.uleb128 0x1e
	.long	.LASF123
	.byte	0x7
	.value	0x2f7
	.long	0xa56
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x1e
	.long	.LASF124
	.byte	0x7
	.value	0x14c
	.long	0xa6d
	.uleb128 0xd
	.long	0x85c
	.uleb128 0xd
	.long	0x5aa
	.byte	0
	.uleb128 0x20
	.long	.LASF125
	.byte	0x7
	.value	0x150
	.long	0x37
	.long	0xa92
	.uleb128 0xd
	.long	0x85c
	.uleb128 0xd
	.long	0x5aa
	.uleb128 0xd
	.long	0x37
	.uleb128 0xd
	.long	0x580
	.byte	0
	.uleb128 0x22
	.long	.LASF127
	.byte	0x7
	.byte	0xc3
	.long	0x85c
	.uleb128 0x1f
	.long	.LASF128
	.byte	0x7
	.byte	0xd1
	.long	0x5aa
	.long	0xab2
	.uleb128 0xd
	.long	0x5aa
	.byte	0
	.uleb128 0x20
	.long	.LASF129
	.byte	0x7
	.value	0x2be
	.long	0x37
	.long	0xacd
	.uleb128 0xd
	.long	0x37
	.uleb128 0xd
	.long	0x85c
	.byte	0
	.uleb128 0x2
	.byte	0x10
	.byte	0x5
	.long	.LASF130
	.uleb128 0x15
	.byte	0x8
	.long	0x16c
	.uleb128 0x23
	.byte	0x8
	.long	0x53f
	.uleb128 0x23
	.byte	0x8
	.long	0x16c
	.uleb128 0x15
	.byte	0x8
	.long	0x544
	.uleb128 0x15
	.byte	0x8
	.long	0x53f
	.uleb128 0x15
	.byte	0x8
	.long	0x549
	.uleb128 0x23
	.byte	0x8
	.long	0xacd
	.uleb128 0x24
	.long	0x185
	.byte	0x2
	.long	0xb0c
	.long	0xb16
	.uleb128 0x25
	.long	.LASF133
	.long	0xb16
	.byte	0
	.uleb128 0x12
	.long	0xad4
	.uleb128 0x26
	.long	0xafe
	.long	.LASF139
	.quad	.LFB328
	.quad	.LFE328-.LFB328
	.uleb128 0x1
	.byte	0x9c
	.long	0xb3e
	.long	0xb47
	.uleb128 0x27
	.long	0xb0c
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x28
	.long	.LASF140
	.byte	0x2
	.byte	0x4
	.long	0x37
	.quad	.LFB326
	.quad	.LFE326-.LFB326
	.uleb128 0x1
	.byte	0x9c
	.long	0xbc1
	.uleb128 0x29
	.long	.LASF131
	.byte	0x2
	.byte	0x4
	.long	0x37
	.uleb128 0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x29
	.long	.LASF132
	.byte	0x2
	.byte	0x4
	.long	0xbc1
	.uleb128 0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x2a
	.quad	.LBB2
	.quad	.LBE2-.LBB2
	.uleb128 0x2b
	.string	"aa"
	.byte	0x2
	.byte	0x6
	.long	0x16c
	.uleb128 0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x2b
	.string	"ab"
	.byte	0x2
	.byte	0x6
	.long	0x16c
	.uleb128 0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x2b
	.string	"av"
	.byte	0x2
	.byte	0x7
	.long	0xacd
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.byte	0
	.uleb128 0x15
	.byte	0x8
	.long	0x5aa
	.uleb128 0x2c
	.long	0x266
	.quad	.LFB348
	.quad	.LFE348-.LFB348
	.uleb128 0x1
	.byte	0x9c
	.long	0xbe6
	.long	0xc01
	.uleb128 0x2d
	.long	.LASF133
	.long	0xb16
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x2e
	.string	"__i"
	.byte	0x1
	.byte	0xb6
	.long	0xacd
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.uleb128 0x2f
	.long	0x32a
	.quad	.LFB349
	.quad	.LFE349-.LFB349
	.uleb128 0x1
	.byte	0x9c
	.long	0xc20
	.long	0xc5a
	.uleb128 0x2d
	.long	.LASF133
	.long	0xc5a
	.uleb128 0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x2e
	.string	"_m"
	.byte	0x1
	.byte	0xce
	.long	0x9d
	.uleb128 0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x2a
	.quad	.LBB3
	.quad	.LBE3-.LBB3
	.uleb128 0x2b
	.string	"tmp"
	.byte	0x1
	.byte	0xd0
	.long	0xacd
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.byte	0
	.byte	0
	.uleb128 0x12
	.long	0xaec
	.uleb128 0x2f
	.long	0x2aa
	.quad	.LFB350
	.quad	.LFE350-.LFB350
	.uleb128 0x1
	.byte	0x9c
	.long	0xc7e
	.long	0xc8b
	.uleb128 0x2d
	.long	.LASF133
	.long	0xc5a
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x2c
	.long	0x22c
	.quad	.LFB351
	.quad	.LFE351-.LFB351
	.uleb128 0x1
	.byte	0x9c
	.long	0xcaa
	.long	0xcb7
	.uleb128 0x2d
	.long	.LASF133
	.long	0xc5a
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.byte	0
	.uleb128 0x2c
	.long	0x4db
	.quad	.LFB352
	.quad	.LFE352-.LFB352
	.uleb128 0x1
	.byte	0x9c
	.long	0xcd6
	.long	0xd10
	.uleb128 0x2d
	.long	.LASF133
	.long	0xb16
	.uleb128 0x2
	.byte	0x91
	.sleb128 -24
	.uleb128 0x30
	.string	"__e"
	.byte	0x1
	.value	0x115
	.long	0xd10
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x30
	.string	"__i"
	.byte	0x1
	.value	0x115
	.long	0xacd
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x30
	.string	"__m"
	.byte	0x1
	.value	0x116
	.long	0x9d
	.uleb128 0x2
	.byte	0x91
	.sleb128 -52
	.byte	0
	.uleb128 0x12
	.long	0xaf8
	.uleb128 0x2f
	.long	0x2e4
	.quad	.LFB353
	.quad	.LFE353-.LFB353
	.uleb128 0x1
	.byte	0x9c
	.long	0xd34
	.long	0xd5c
	.uleb128 0x2d
	.long	.LASF133
	.long	0xb16
	.uleb128 0x2
	.byte	0x91
	.sleb128 -40
	.uleb128 0x2e
	.string	"__i"
	.byte	0x1
	.byte	0xc6
	.long	0xacd
	.uleb128 0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x2e
	.string	"_m"
	.byte	0x1
	.byte	0xc6
	.long	0x9d
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.byte	0
	.uleb128 0x2f
	.long	0x477
	.quad	.LFB354
	.quad	.LFE354-.LFB354
	.uleb128 0x1
	.byte	0x9c
	.long	0xd7b
	.long	0xdc7
	.uleb128 0x2d
	.long	.LASF133
	.long	0xb16
	.uleb128 0x2
	.byte	0x91
	.sleb128 -56
	.uleb128 0x30
	.string	"__e"
	.byte	0x1
	.value	0x107
	.long	0xdc7
	.uleb128 0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x30
	.string	"__i"
	.byte	0x1
	.value	0x107
	.long	0xacd
	.uleb128 0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x30
	.string	"__s"
	.byte	0x1
	.value	0x107
	.long	0x9d
	.uleb128 0x3
	.byte	0x91
	.sleb128 -84
	.uleb128 0x30
	.string	"__f"
	.byte	0x1
	.value	0x108
	.long	0x9d
	.uleb128 0x3
	.byte	0x91
	.sleb128 -88
	.byte	0
	.uleb128 0x12
	.long	0xaf8
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x39
	.byte	0x1
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x8
	.byte	0
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x18
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.uleb128 0x32
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2f
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x47
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x47
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x47
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x64
	.uleb128 0x13
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.long	0xac
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	.LFB328
	.quad	.LFE328-.LFB328
	.quad	.LFB348
	.quad	.LFE348-.LFB348
	.quad	.LFB349
	.quad	.LFE349-.LFB349
	.quad	.LFB350
	.quad	.LFE350-.LFB350
	.quad	.LFB351
	.quad	.LFE351-.LFB351
	.quad	.LFB352
	.quad	.LFE352-.LFB352
	.quad	.LFB353
	.quad	.LFE353-.LFB353
	.quad	.LFB354
	.quad	.LFE354-.LFB354
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.Ltext0
	.quad	.Letext0
	.quad	.LFB328
	.quad	.LFE328
	.quad	.LFB348
	.quad	.LFE348
	.quad	.LFB349
	.quad	.LFE349
	.quad	.LFB350
	.quad	.LFE350
	.quad	.LFB351
	.quad	.LFE351
	.quad	.LFB352
	.quad	.LFE352
	.quad	.LFB353
	.quad	.LFE353
	.quad	.LFB354
	.quad	.LFE354
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF79:
	.string	"_shortbuf"
.LASF54:
	.string	"__off_t"
.LASF61:
	.string	"_IO_read_ptr"
.LASF73:
	.string	"_chain"
.LASF43:
	.string	"_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_order"
.LASF32:
	.string	"exchange"
.LASF10:
	.string	"memory_order_release"
.LASF53:
	.string	"size_t"
.LASF136:
	.string	"/home/fzjiao/source/aompexercise/linkedlist/taggedptr"
.LASF67:
	.string	"_IO_buf_base"
.LASF48:
	.string	"long long unsigned int"
.LASF121:
	.string	"remove"
.LASF93:
	.string	"__mbstate_t"
.LASF40:
	.string	"_ZNVSt6atomicInE21compare_exchange_weakERnnSt12memory_order"
.LASF58:
	.string	"atomic<__int128>"
.LASF117:
	.string	"getc"
.LASF114:
	.string	"fseek"
.LASF47:
	.string	"long long int"
.LASF0:
	.string	"signed char"
.LASF16:
	.string	"_ZNSt6atomicInEaSERKS0_"
.LASF74:
	.string	"_fileno"
.LASF62:
	.string	"_IO_read_end"
.LASF2:
	.string	"long int"
.LASF50:
	.string	"char16_t"
.LASF60:
	.string	"_flags"
.LASF5:
	.string	"unsigned int"
.LASF68:
	.string	"_IO_buf_end"
.LASF77:
	.string	"_cur_column"
.LASF91:
	.string	"__count"
.LASF21:
	.string	"_ZNSt6atomicInEaSEn"
.LASF76:
	.string	"_old_offset"
.LASF81:
	.string	"_offset"
.LASF133:
	.string	"this"
.LASF122:
	.string	"rename"
.LASF98:
	.string	"_G_fpos_t"
.LASF28:
	.string	"_ZNVSt6atomicInE5storeEnSt12memory_order"
.LASF96:
	.string	"__pos"
.LASF38:
	.string	"_ZNSt6atomicInE21compare_exchange_weakERnnSt12memory_order"
.LASF33:
	.string	"_ZNSt6atomicInE8exchangeEnSt12memory_order"
.LASF12:
	.string	"memory_order_seq_cst"
.LASF99:
	.string	"_IO_marker"
.LASF115:
	.string	"fsetpos"
.LASF90:
	.string	"__wchb"
.LASF39:
	.string	"compare_exchange_strong"
.LASF130:
	.string	"__int128"
.LASF34:
	.string	"_ZNVSt6atomicInE8exchangeEnSt12memory_order"
.LASF19:
	.string	"_ZNKSt6atomicInEcvnEv"
.LASF6:
	.string	"long unsigned int"
.LASF106:
	.string	"ferror"
.LASF65:
	.string	"_IO_write_ptr"
.LASF118:
	.string	"gets"
.LASF25:
	.string	"_ZNVKSt6atomicInE12is_lock_freeEv"
.LASF101:
	.string	"_sbuf"
.LASF42:
	.string	"_ZNVSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_"
.LASF128:
	.string	"tmpnam"
.LASF134:
	.string	"GNU C++ 4.8.4 -mcx16 -mtune=generic -march=x86-64 -g -std=c++11 -fstack-protector"
.LASF26:
	.string	"store"
.LASF59:
	.string	"_IO_FILE"
.LASF69:
	.string	"_IO_save_base"
.LASF89:
	.string	"__wch"
.LASF108:
	.string	"fgetc"
.LASF49:
	.string	"wchar_t"
.LASF45:
	.string	"bool"
.LASF80:
	.string	"_lock"
.LASF75:
	.string	"_flags2"
.LASF87:
	.string	"_mode"
.LASF110:
	.string	"fgets"
.LASF7:
	.string	"memory_order_relaxed"
.LASF18:
	.string	"operator __int128"
.LASF52:
	.string	"memory_order"
.LASF29:
	.string	"load"
.LASF120:
	.string	"perror"
.LASF24:
	.string	"_ZNKSt6atomicInE12is_lock_freeEv"
.LASF36:
	.string	"_ZNSt6atomicInE21compare_exchange_weakERnnSt12memory_orderS2_"
.LASF56:
	.string	"sizetype"
.LASF37:
	.string	"_ZNVSt6atomicInE21compare_exchange_weakERnnSt12memory_orderS2_"
.LASF119:
	.string	"clearerr"
.LASF139:
	.string	"_ZNSt6atomicInEC2Ev"
.LASF9:
	.string	"memory_order_acquire"
.LASF66:
	.string	"_IO_write_end"
.LASF129:
	.string	"ungetc"
.LASF95:
	.string	"9_G_fpos_t"
.LASF138:
	.string	"_IO_lock_t"
.LASF20:
	.string	"_ZNVKSt6atomicInEcvnEv"
.LASF137:
	.string	"_M_i"
.LASF111:
	.string	"fopen"
.LASF102:
	.string	"_pos"
.LASF107:
	.string	"fflush"
.LASF72:
	.string	"_markers"
.LASF97:
	.string	"__state"
.LASF11:
	.string	"memory_order_acq_rel"
.LASF44:
	.string	"_ZNVSt6atomicInE23compare_exchange_strongERnnSt12memory_order"
.LASF27:
	.string	"_ZNSt6atomicInE5storeEnSt12memory_order"
.LASF124:
	.string	"setbuf"
.LASF3:
	.string	"unsigned char"
.LASF15:
	.string	"operator="
.LASF1:
	.string	"short int"
.LASF78:
	.string	"_vtable_offset"
.LASF57:
	.string	"FILE"
.LASF51:
	.string	"char32_t"
.LASF112:
	.string	"fread"
.LASF105:
	.string	"feof"
.LASF8:
	.string	"memory_order_consume"
.LASF92:
	.string	"__value"
.LASF46:
	.string	"char"
.LASF132:
	.string	"argv"
.LASF22:
	.string	"_ZNVSt6atomicInEaSEn"
.LASF41:
	.string	"_ZNSt6atomicInE23compare_exchange_strongERnnSt12memory_orderS2_"
.LASF30:
	.string	"_ZNKSt6atomicInE4loadESt12memory_order"
.LASF103:
	.string	"fpos_t"
.LASF131:
	.string	"argc"
.LASF127:
	.string	"tmpfile"
.LASF104:
	.string	"fclose"
.LASF113:
	.string	"freopen"
.LASF100:
	.string	"_next"
.LASF55:
	.string	"__off64_t"
.LASF63:
	.string	"_IO_read_base"
.LASF71:
	.string	"_IO_save_end"
.LASF4:
	.string	"short unsigned int"
.LASF126:
	.string	"getchar"
.LASF82:
	.string	"__pad1"
.LASF83:
	.string	"__pad2"
.LASF84:
	.string	"__pad3"
.LASF85:
	.string	"__pad4"
.LASF86:
	.string	"__pad5"
.LASF94:
	.string	"11__mbstate_t"
.LASF88:
	.string	"_unused2"
.LASF31:
	.string	"_ZNVKSt6atomicInE4loadESt12memory_order"
.LASF17:
	.string	"_ZNVSt6atomicInEaSERKS0_"
.LASF135:
	.string	"cx16test.cpp"
.LASF70:
	.string	"_IO_backup_base"
.LASF125:
	.string	"setvbuf"
.LASF35:
	.string	"compare_exchange_weak"
.LASF23:
	.string	"is_lock_free"
.LASF123:
	.string	"rewind"
.LASF116:
	.string	"ftell"
.LASF109:
	.string	"fgetpos"
.LASF140:
	.string	"main"
.LASF64:
	.string	"_IO_write_base"
.LASF14:
	.string	"~atomic"
.LASF13:
	.string	"atomic"
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04.1) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
