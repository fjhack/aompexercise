#include <atomic>
//#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <iostream>

using namespace std;

void print_128bit(char* address) {
    char* byte_array = address;
    printf("foo ");
    int i = 0;
    while (i<4) {
	printf("%02X", (int)byte_array[i]);
	i++;
    }
    printf("\n");
}


// Decimal print
/*
std::ostream&
operator<<( std::ostream& dest, __int128_t value )
{
    std::ostream::sentry s( dest );
    if ( s ) {
        __uint128_t tmp = value < 0 ? -value : value;
        char buffer[ 128 ];
        char* d = std::end( buffer );
        do
        {
            -- d;
            *d = "0123456789"[ tmp % 10 ];
            tmp /= 10;
        } while ( tmp != 0 );
        if ( value < 0 ) {
            -- d;
            *d = '-';
        }
        int len = std::end( buffer ) - d;
        if ( dest.rdbuf()->sputn( d, len ) != len ) {
            dest.setstate( std::ios_base::badbit );
        }
    }
    return dest;
}
*/

std::ostream&
operator<<( std::ostream& dest, __int128_t value )
{
    std::ostream::sentry s( dest );
    if ( s ) {
        __uint128_t tmp = value < 0 ? -value : value;
        char buffer[ 128 ];
        char* d = std::end( buffer );
        do
        {
            -- d;
            *d = "0123456789abcdef"[ tmp % 16 ];
            tmp /= 16;
        } while ( tmp != 0 );
        if ( value < 0 ) {
            -- d;
            *d = '-';
        }
        int len = std::end( buffer ) - d;
        if ( dest.rdbuf()->sputn( d, len ) != len ) {
            dest.setstate( std::ios_base::badbit );
        }
    }
    return dest;
}


__int128 mark_ptr(void *addr, bool mark) {
    return (((__int128)mark << 64) | (__int128)addr);
}

void* getaddr(__int128 taggedptr) {
    return (void *)(taggedptr << 64 >> 64);
}

bool getmark(__int128 taggedptr) {
    return (bool)(taggedptr >> 64);
}


int main(int argc, char** argv)
{

    int ia, ib;
    int *pia, *pib;
    bool mark;

    __int128 tpa __attribute__((aligned(16)));

    int *addrint, *addrint2;
    long *addrlng;
    __int128 *addr128 __attribute__((aligned(16)));


    atomic<__int128> aa __attribute__((aligned(16)));

    pia = (int *)malloc(sizeof(int));
    
    mark = 1;

    cout << sizeof(ia) << ", " << sizeof(pia) << endl;
    cout << pia << ": " << *pia << endl;

    addr128 = (__int128 *)malloc(sizeof(__int128));
    addrint = (int *)malloc(sizeof(int));
    addrint2 = (int *)malloc(sizeof(int)+3);

    // *addr128 = ((__int128)true << 64) | (__int128)addrint2;
    *addr128 = mark_ptr(addrint2, true);

    // assertation for alignment
    cout << addr128 << "\t" << (reinterpret_cast<uintptr_t>(addr128) % sizeof(*addr128)) << endl;
    cout << addr128 << "\t" << (reinterpret_cast<uintptr_t>(addr128) & (sizeof(*addr128)-1)) << endl;

    cout << addrint << "\t" << (reinterpret_cast<uintptr_t>(addrint) % sizeof(*addrint)) << endl;
    cout << addrint << "\t" << (reinterpret_cast<uintptr_t>(addrint) & (sizeof(*addrint)-1)) << endl;

    cout << addrint2 << "\t" << (reinterpret_cast<uintptr_t>(addrint2) % sizeof(*addrint)) << endl;
    cout << addrint2 << "\t" << (reinterpret_cast<uintptr_t>(addrint2) & (sizeof(*addrint)-1)) << endl;

    cout << sizeof(addr128) << endl;
    cout << *addr128 << "\t" << true << endl;

    cout << getaddr(*addr128) << "\t" << getmark(*addr128) << endl;
    free(addr128);
    free(addrint);
    free(addrint2);
}


