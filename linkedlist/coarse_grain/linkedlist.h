#include <cstdio>
#include <cstdlib>
#include <climits>
#include <iostream>
#include <mutex>
#include <vector>

template <typename T>
class lnode {
public:
    int key;
    T value;
    lnode<T> *next;
};

template <typename T>
class linkedlist {
    lnode<T> *head;
    std::mutex c_lock;
public:
    linkedlist();
    ~linkedlist();
    bool add(int key, T item);
    bool add1(int key);
    bool remove(int key);
    bool contain(int key);
    T getv(int key);
    void key_to_vector(std::vector<int> &outvec);
    void printll();
    int count();
};

#include "linkedlist.cpp"
