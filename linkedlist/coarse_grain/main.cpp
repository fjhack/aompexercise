#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <random>
#include <thread>
#include <vector>

#include "linkedlist.h"

void tbody(int tid) 
{
    std::cout << "[Thread " << tid << "]  ";
}

void spawn_threads(int numthreads)
{
    std::vector<std::thread> ts;
    for (int i=0; i<numthreads; i++) {
	ts.push_back(std::thread(tbody, i));
    }
    
    for (auto& t:ts) t.join();
    std::cout << std::endl;
}


/* simple test 1: sum up all numbers in a linked list */
int sum_ll(linkedlist<int> *L, int lsize)
{
    int s = 0;
    for (int i=0; i<lsize; i++) {
	s += L->getv(i);
    }
    std::cout << s << std::endl;
    return s;
}


/* simple test 2: generate a linked list containing fibonacci numbers */
int fibM(int n, linkedlist<int> *L)
{
    if (n == 1 || n == 2) {
	L->add(n, 1);
	return 1;
    }
    else {
	int v;
	if (L->contain(n)) return L->getv(n);
	v = fibM(n-1, L) + fibM(n-2, L);
	L->add(n, v);
	return v;
    }
}

void fibtest(int n, linkedlist<int> *L)
{
    std::cout << fibM(n, L) << std::endl;
    L->printll();
}


/*
   Multi threading test 1:
   In a small linked list, while each node was initiated with 1, using a loop to randomly add 1 on any node, total N times. Verify the sum of all value has increased N.
 */
void adder_thread(linkedlist<int> *L, int iter, int key_range)
{
    std::mt19937 rng;
    rng.seed(std::random_device()());
    int cv, rk;
    std::uniform_int_distribution<std::mt19937::result_type> dist6(0, key_range-1);
    for (int i=0; i<iter; i++) {
	rk = dist6(rng);
	if (rk == key_range) std::cout << "FALSE" << std::endl;
	// cv = L->getv(rk);
	L->add1(rk);
    }    
}

void multi_thread_ll_test(int numthreads, int lsize)
{
    linkedlist<int> mls;
    int i;
    int iter = 10000000;
    int ev = 0;

    for (i=0; i<lsize; i++) {
	mls.add(i, 1);
    }

    std::cout << "----- BEFORE -----" << std::endl;
    mls.printll();
    ev = sum_ll(&mls, lsize);
    std::cout << std::endl;

    std::vector<std::thread> ts;
    for (int i=0; i<numthreads; i++) {
	ts.push_back(std::thread(adder_thread, &mls, iter, lsize));
    }
    
    for (auto& t:ts) t.join();

    ev = ev + iter*numthreads;

    std::cout << "----- AFTER -----" << std::endl;
    mls.printll();
    sum_ll(&mls, lsize);
    std::cout << ev << std::endl;

}

/*
  Multi threading test 2:
  Given a long array (N elements), which each node have key sequentially from 0 to N-1, and M threads. First, generate an operation (add and remove of nodes) sequence for each thread, each have L operations, each operation is adding or removing a node with a random key K, which K can be divided by thread ID. Validation run will replay all these operation sequentially, and compare the result when replaying this sequence in parallel.

  1. test simutaneous operations, add only, into an empty list
  2. test simutaneous operations, remove only
 */

void generate_seq(int numthreads, int lsize, int seqsize, std::vector<std::vector<int>> &opseqs)
{
    std::mt19937 rng;
    rng.seed(std::random_device()());
    int key_range_base = lsize / numthreads;
    std::uniform_int_distribution<std::mt19937::result_type> dist6(0, key_range_base-1);
    int rv;
    srand(time(0));

    for (int i=0; i<numthreads; i++) {
	for (int j=0; j<seqsize; j++) {
	    rv = dist6(rng) * numthreads + i;
	    while ( (std::find(opseqs[i].begin(), opseqs[i].end(), rv) != opseqs[i].end()) || (std::find(opseqs[i].begin(), opseqs[i].end(), -rv) != opseqs[i].end()) ) {
		rv = dist6(rng) * numthreads + i;
	    }
	    opseqs[i][j] = rv;

	    // For now, we are not playing add and remove simutaneously
	    // if (rand() % 2 == 1) {
	    // 	opseqs[i][j] = - opseqs[i][j];
	    // }
	}
    }
}

void replay_seq_all(int numthreads, int seqsize, linkedlist<int> *L, std::vector<std::vector<int>> &opseqs)
{
    /* replay all sequence in serial, use result for verification */
    int i, j;
    for (i=0; i<numthreads; i++) {
	for (j=0; j<seqsize; j++) {
	    if (opseqs[i][j] >= 0) {
		L->add(opseqs[i][j], 1);
	    } else {
		L->remove(-opseqs[i][j]);
	    }
	}
    }
}

void replay_adder_thread(int seqsize, linkedlist<int> *L, std::vector<int> opseq)
{
    int i, j;
    for (i=0; i<seqsize; i++) {
	if (opseq[i] >= 0) {
	    L->add(opseq[i], 1);
	} else {
	    L->remove(-opseq[i]);
	}
    }
}

void multithread_add_remove(int numthreads, int lsize, int seqsize)
{
    int i, j;
    std::vector<std::vector<int>> opseqs(numthreads, std::vector<int>(seqsize));
    linkedlist<int> st_ll, mt_ll;
    std::vector<int> keyvec, keyvec_m;

    st_ll.printll();

    generate_seq(numthreads, lsize, seqsize, opseqs);

    /* print generated sequence */
    for (i=0; i<numthreads; i++) {
	for (j=0; j<seqsize; j++) {
	    std::cout << opseqs[i][j] << ", ";
	}
	std::cout << std::endl;
    }

    /* single thread - for comparison */
    replay_seq_all(numthreads, seqsize, &st_ll, opseqs);
    st_ll.key_to_vector(keyvec);
    st_ll.printll();

    for (i=0; i<keyvec.size(); i++) {
	std::cout << keyvec[i] << " ";
    }
    std::cout << std::endl;

    /* multi thread replay */
    std::vector<std::thread> ts;
    for (i=0; i<numthreads; i++) {
	ts.push_back(std::thread(replay_adder_thread, seqsize, &mt_ll, opseqs[i]));
    }
    
    for (auto& t:ts) t.join();
    std::cout << std::endl << std::endl;
    mt_ll.key_to_vector(keyvec_m);
    mt_ll.printll();

    for (i=0; i<keyvec_m.size(); i++) {
	std::cout << keyvec_m[i] << " ";
    }
    std::cout << std::endl;
    
}


int main(int argc, char** argv)
{
    int i;
    int numthreads;

    linkedlist<int> ll, L2;
    ll.add(1, 2);
    ll.add(3, 4);
    ll.add(5, 6);
    ll.printll();

    numthreads = atoi(argv[1]);

    // for (i=1; i<10; i++) {
    // 	std::cout << fib(i, &L2) << ' ';
    // }
    // std::cout << std::endl;

    fibtest(10, &L2);
    std::cout << ll.count() << "\t" << L2.count() << std::endl;

    multi_thread_ll_test(numthreads, 16);

    // multithread_add_remove(numthreads, 48, 16);

    return 0;
}
