template <typename T>
linkedlist<T>::linkedlist()
{
    lnode<T> *new_tail;
    head = new lnode<T>;
    new_tail = new lnode<T>;
    head->next = new_tail;
    head->key = INT_MIN;
    head->next->key = INT_MAX;
    head->next->next = NULL;
}

template <typename T>
linkedlist<T>::~linkedlist()
{
    lnode<T> *curr, *prev;
    prev = head;
    curr = head->next;
    while (curr != NULL) {
	prev = curr;
	curr = curr->next;
	delete prev;
    }
    delete head;
}

// UNSAFE
template <typename T>
bool linkedlist<T>::add(int key, T item)
{
    lnode<T> *curr, *prev, *newnode;
    curr = head;
    while (curr->next != NULL) {
	prev = curr;
	curr = curr->next;
	if (curr->key < key) {
	    continue;
	}
	else if (curr->key == key) {
	    curr->value = item;
	    return true;
	} else {
	    newnode = new lnode<T>;
	    newnode->key = key;
	    newnode->value = item;
	    prev->next = newnode;
	    newnode->next = curr;
	    return true;
	}
    }
    return false;
}

template <typename T>
inline bool linkedlist<T>::validate(lnode<T> *prev, lnode<T> *curr)
{
    lnode<T> *lprev, *lcurr;

    lcurr = head;

    while (lcurr->next != NULL) {
	if (lcurr == prev) {
	    if (lcurr->next == curr) {
		return true;
	    }
	    else {
		return false;
	    }
	}
	lprev = lcurr;
	lcurr = lcurr->next;
    }
    return false;
}


// This is only for test usage - since if we split into two calls getv() and add(),
// it's still not enough to guarantee the atomicity.

template <typename T>
bool linkedlist<T>::add1(int key)
{
    lnode<T> *curr, *prev, *newnode;
    bool validated;

    // head->nodelock.lock();
    curr = head;

    while (curr->next != NULL) {
	prev = curr;
	curr = curr->next;

	// prev->nodelock.unlock();
	// curr->nodelock.lock();

	if (curr->key < key) {
	    continue;
	}
	else if (curr->key == key) {
	    prev->nodelock.lock();
	    curr->nodelock.lock();
	    validated = validate(prev, curr);
	    if (validated) {
		curr->value += 1;
		prev->nodelock.unlock();
		curr->nodelock.unlock();
		return true;
	    }
	    else {
		prev->nodelock.unlock();
		curr->nodelock.unlock();
		return false;
	    }
	}
	else {
	    return false;
	}
	// else if (curr->key == key) {
	//     curr->value += 1;
	//     // curr->nodelock.unlock();
	//     return true;
	// } else {
	//     // curr->nodelock.unlock();
	//     return false;
	// }
    }
}

// UNSAFE
template <typename T>
bool linkedlist<T>::remove(int key)
{
    lnode<T> *curr, *prev;
    curr = head;
    while (curr->key < key) {
	prev = curr;
	curr = curr->next;
    }
    if (curr->key == key) {
	prev->next = curr->next;
	delete curr;
	return true;
    }
    else {
	return false;
    }
}

// UNSAFE
template <typename T>
bool linkedlist<T>::contain(int key)
{
    lnode<T> *curr, *prev;
    curr = head;
    while (curr->key < key) {
	prev = curr;
	curr = curr->next;
    }
    if (curr->key == key) {
	return true;
    }
    else {
	return false;
    }
}

template <typename T>
T linkedlist<T>::getv(int key)
{
    lnode<T> *curr, *prev;
    curr = head;
    while (curr->key < key) {
	prev = curr;
	curr = curr->next;
    }
    if (curr->key == key) {
	return curr->value;
    }
    else {
	return static_cast<T>(NULL);
    }
}

// UNSAFE
template <typename T>
void linkedlist<T>::printll()
{
    lnode<T> *curr;
    curr = head;
    while (curr != NULL) {
	std::cout << "[" << curr << ": <" << curr->key << ": " << curr->value << ">]";
	curr = curr->next;
	if (curr != NULL) {
	    std::cout << " -> ";
	}
	else {
	    std::cout << std::endl;
	}
    }
}

// UNSAFE
template <typename T>
int linkedlist<T>::count()
{
    lnode<T> *curr;
    int c = 0;
    curr = head->next;
    while (curr->next != NULL) {
	c++;
	curr = curr->next;
    }
    return c;
}
