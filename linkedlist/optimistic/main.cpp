#include <cstdlib>
#include <ctime>
#include <random>
#include <thread>
#include <vector>

#include "linkedlist.h"

void tbody(int tid) 
{
    std::cout << "[Thread " << tid << "]  ";
}

void spawn_threads(int numthreads)
{
    std::vector<std::thread> ts;
    for (int i=0; i<numthreads; i++) {
	ts.push_back(std::thread(tbody, i));
    }
    
    for (auto& t:ts) t.join();
    std::cout << std::endl;
}

int sum_ll(linkedlist<int> *L, int lsize)
{
    int s = 0;
    for (int i=0; i<lsize; i++) {
	s += L->getv(i);
    }
    std::cout << s << std::endl;
    return s;
}

void adder_thread(linkedlist<int> *L, int iter, int key_range)
{
    std::mt19937 rng;
    rng.seed(std::random_device()());
    int cv, rk;
    std::uniform_int_distribution<std::mt19937::result_type> dist6(0, key_range-1);
    for (int i=0; i<iter; i++) {
	rk = dist6(rng);
	if (rk == key_range) std::cout << "FALSE" << std::endl;
	// cv = L->getv(rk);
	L->add1(rk);
    }    
}

void multi_thread_ll_test(int numthreads, int lsize)
{
    linkedlist<int> mls;
    int i;
    int iter = 10000000;
    int ev = 0;

    for (i=0; i<lsize; i++) {
	mls.add(i, 1);
    }

    std::cout << "----- BEFORE -----" << std::endl;
    mls.printll();
    ev = sum_ll(&mls, lsize);
    std::cout << std::endl;

    std::vector<std::thread> ts;
    for (int i=0; i<numthreads; i++) {
	ts.push_back(std::thread(adder_thread, &mls, iter, lsize));
    }
    
    for (auto& t:ts) t.join();

    ev = ev + iter*numthreads;

    std::cout << "----- AFTER -----" << std::endl;
    mls.printll();
    sum_ll(&mls, lsize);
    std::cout << ev << std::endl;

}

int fibM(int n, linkedlist<int> *L)
{
    if (n == 1 || n == 2) {
	L->add(n, 1);
	return 1;
    }
    else {
	int v;
	if (L->contain(n)) return L->getv(n); 
	v = fibM(n-1, L) + fibM(n-2, L);
	L->add(n, v);
	return v;
    }
}

void fibtest(int n, linkedlist<int> *L)
{
    std::cout << fibM(n, L) << std::endl;
    L->printll();    
}

int main(int argc, char** argv)
{
    int i;
    int numthreads;

    linkedlist<int> ll, L2;
    ll.add(1, 2);
    ll.add(3, 4);
    ll.add(5, 6);
    ll.printll();

    numthreads = atoi(argv[1]);

    // for (i=1; i<10; i++) {
    // 	std::cout << fib(i, &L2) << ' ';
    // }
    // std::cout << std::endl;

    fibtest(10, &L2);
    std::cout << ll.count() << "\t" << L2.count() << std::endl;

    spawn_threads(numthreads);

    multi_thread_ll_test(numthreads, 16);

    return 0;
}
