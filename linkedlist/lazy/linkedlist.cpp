template <typename T>
linkedlist<T>::linkedlist()
{
    lnode<T> *new_tail;
    head = new lnode<T>;
    new_tail = new lnode<T>;
    head->next = new_tail;
    head->key = INT_MIN;
    head->next->key = INT_MAX;
    head->next->next = NULL;
}

template <typename T>
linkedlist<T>::~linkedlist()
{
    lnode<T> *curr, *prev;
    prev = head;
    curr = head->next;
    while (curr != NULL) {
	prev = curr;
	curr = curr->next;
	delete prev;
    }
    delete head;
}

template <typename T>
bool linkedlist<T>::add(int key, T item)
{
    lnode<T> *curr, *prev, *newnode;
    while (1) {
        prev = head;
	curr = head->next;

	while (curr->key < key) {
	    prev = curr;
	    curr = curr->next;
	}
	prev->nodelock.lock();
	try {
	    curr->nodelock.lock();
	    try {
	        if (validate(prev, curr)) {
		    if (curr->key == key) {
			curr->nodelock.unlock();
			prev->nodelock.unlock();
		        return false;
		    }
		    else {
		        newnode = new lnode<T>;
			newnode->key = key;
			newnode->value = item;
			prev->next = newnode;
			newnode->next = curr;

			curr->nodelock.unlock();
			prev->nodelock.unlock();
			return true;
		    }
		}
	    }
	    catch (...) {
	        curr->nodelock.unlock();
		throw;
	    }
	    curr->nodelock.unlock();
	}
	catch (...) {
	    prev->nodelock.unlock();
	    throw;
	}
	prev->nodelock.unlock();
    }
}


template <typename T>
inline bool linkedlist<T>::validate(lnode<T> *prev, lnode<T> *curr)
{
    return !(prev->removed) && !(curr->removed) && (prev->next == curr);
}


// This is only for test usage - since if we split into two calls getv() and add(),
// it's still not enough to guarantee the atomicity.

template <typename T>
bool linkedlist<T>::add1(int key)
{
    lnode<T> *curr, *prev, *newnode;
    while (1) {
        prev = head;
	curr = head->next;

	while (curr->key < key) {
	    prev = curr;
	    curr = curr->next;
	}
	prev->nodelock.lock();
	try {
	    curr->nodelock.lock();
	    try {
	        if (validate(prev, curr)) {
		    if (curr->key != key) {
			curr->nodelock.unlock();
			prev->nodelock.unlock();
		        return false;
		    }
		    else {
		        curr->value += 1;
			curr->nodelock.unlock();
			prev->nodelock.unlock();
			return true;
		    }
		}
	    }
	    catch (...) {
	        curr->nodelock.unlock();
		throw;
	    }
	    curr->nodelock.unlock();
	}
	catch (...) {
	    prev->nodelock.unlock();
	    throw;
	}
	prev->nodelock.unlock();
    }
}


template <typename T>
bool linkedlist<T>::remove(int key)
{
    lnode<T> *curr, *prev;
    while (1) {
        prev = head;
	curr = prev->next;
	while (curr->key < key) {
	    prev = curr;
	    curr = curr->next;
	}
	prev->nodelock.lock();
	try {
	    curr->nodelock.lock();
	    try {
	        if (validate(prev, curr)) {
		    if (curr->key != key) {
			curr->nodelock.unlock();
			prev->nodelock.unlock();
		        return false;
		    }
		    else {
		        curr->removed = true;
			prev->next = curr->next;
			delete curr;
			// Hmm...
			// curr->nodelock.unlock();
			prev->nodelock.unlock();
			return true;
		    }
		}
	    }
	    catch (...) {
	        curr->nodelock.unlock();
		throw;
	    }
	    curr->nodelock.unlock();
	}
	catch (...) {
	    prev->nodelock.unlock();
	    throw;
	}
	prev->nodelock.unlock();
    }
}


// UNSAFE
template <typename T>
bool linkedlist<T>::contain(int key)
{
    lnode<T> *curr, *prev;
    curr = head;
    while (curr->key < key) {
	prev = curr;
	curr = curr->next;
    }
    if (curr->key == key) {
	return true;
    }
    else {
	return false;
    }
}

template <typename T>
T linkedlist<T>::getv(int key)
{
    lnode<T> *curr, *prev;
    curr = head;
    while (curr->key < key) {
	prev = curr;
	curr = curr->next;
    }
    if (curr->key == key) {
	return curr->value;
    }
    else {
	return static_cast<T>(NULL);
    }
}

// UNSAFE
template <typename T>
void linkedlist<T>::printll()
{
    lnode<T> *curr;
    curr = head;
    while (curr != NULL) {
	std::cout << "[" << curr << ": <" << curr->key << ": " << curr->value << ">]";
	curr = curr->next;
	if (curr != NULL) {
	    std::cout << " -> ";
	}
	else {
	    std::cout << std::endl;
	}
    }
}

// UNSAFE
template <typename T>
int linkedlist<T>::count()
{
    lnode<T> *curr;
    int c = 0;
    curr = head->next;
    while (curr->next != NULL) {
	c++;
	curr = curr->next;
    }
    return c;
}
