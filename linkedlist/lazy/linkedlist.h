#include <cstdio>
#include <cstdlib>
#include <climits>
#include <iostream>
#include <mutex>

template <typename T>
class lnode {
public:
    int key;
    T value;
    std::mutex nodelock;
    lnode<T> *next;
    bool removed;

    lnode(): removed(false) {};
};

template <typename T>
class linkedlist {
    lnode<T> *head;
    std::mutex c_lock;
    bool validate(lnode<T> *prev, lnode<T> *curr);
public:
    linkedlist();
    ~linkedlist();
    bool add(int key, T item);
    bool add1(int key);
    bool remove(int key);
    bool contain(int key);
    T getv(int key);
    void printll();
    int count();
};

#include "linkedlist.cpp"
